const locations = [
  {
    name: 'Fort Lauderdale, FL',
    description: `Fort Lauderdale is a city on Florida's southeastern coast, known for its beaches and boating canals. The Strip is a promenade running along oceanside highway A1A. It's lined with upscale outdoor restaurants, bars, boutiques and luxury hotels. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Miami, FL',
    description: `Miami, officially the City of Miami, is an American city that is the seat of Miami-Dade County, and is the cultural, economic and financial center of South Florida. The city covers an area of about 56 square miles (150 km2) between the Everglades to the west and Biscayne Bay to the east.`,
    img: 'city_search_2.png'
  },
  {
    name: 'New York City, NY',
    description: `New York City comprises 5 boroughs sitting where the Hudson River meets the Atlantic Ocean. At its core is Manhattan, a densely populated borough that’s among the world’s major commercial, financial and cultural centers.`,
    img: 'city_search_2.png'
  },
  {
    name: 'San Diego, CA',
    description: `San Diego is a city on the Pacific coast of California known for its beaches, parks and warm climate. Immense Balboa Park is the site of the renowned San Diego Zoo, as well as numerous art galleries, artist studios, museums and gardens.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Portland, OR',
    description: `Portland, Oregon’s largest city, sits on the Columbia and Willamette rivers, in the shadow of snow-capped Mount Hood. It’s known for its parks, bridges and bicycle paths, as well as for its eco-friendliness and its microbreweries and coffeehouses.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Chicago, IL',
    description: `Chicago, on Lake Michigan in Illinois, is among the largest cities in the U.S. Famed for its bold architecture, it has a skyline punctuated by skyscrapers such as the iconic John Hancock Center, 1,451-ft. Willis Tower (formerly the Sears Tower) and the neo-Gothic Tribune Tower.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Orlando, FL',
    description: `Orlando, a city in central Florida, is home to more than a dozen theme parks. Chief among its claims to fame is Walt Disney World, comprised of parks like the Magic Kingdom and Epcot, as well as water parks. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Duluth, MN',
    description: `Duluth is a port city on Lake Superior in Minnesota. The waterfront Lakewalk trail passes along Canal Park, with views of the 1905 Aerial Lift Bridge. The landmark connects the city to the Park Point sandbar. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Newark, NJ',
    description: `Newark is a New Jersey city, home to Newark Liberty International Airport. The New Jersey Performing Arts Center (NJPAC) hosts big-name concerts, dance performances and other shows. Newark Museum’s broad art collection features American paintings and sculptures.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Des Moines, IO',
    description: `Des Moines is the capital city of Iowa. The gold-domed Iowa State Capitol building is among the 19th- and early-20th-century landmarks of the East Village area. The Des Moines Art Center is noted for its contemporary collections and Pappajohn Sculpture Park.`,
    img: 'city_search_2.png'
  },
  {
    name: 'New Orleans, LA',
    description: `New Orleans is a Louisiana city on the Mississippi River, near the Gulf of Mexico. Nicknamed the "Big Easy," it's known for its round-the-clock nightlife, vibrant live-music scene and spicy, singular cuisine reflecting its history as a melting pot of French, African and American cultures.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Pittsburgh, PA',
    description: `Pittsburgh is a city in western Pennsylvania at the junction of 3 rivers. Its Gilded Age sites, including the Carnegie Museum of Natural History, the Carnegie Museum of Art and the Phipps Conservatory and Botanical Gardens, speak to its history as an early-20th-century industrial capital. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Austin, TX',
    description: `Austin is the state capital of Texas, an inland city bordering the Hill Country region. Home to the University of Texas flagship campus, Austin is known for its eclectic live-music scene centered around country, blues and rock. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Dallas, TX',
    description: `Dallas, a modern metropolis in north Texas, is a commercial and cultural hub of the region. Downtown’s Sixth Floor Museum at Dealey Plaza commemorates the site of President John F. Kennedy’s assassination in 1963. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Charleston, SC',
    description: `Charleston, the South Carolina port city founded in 1670, is defined by its cobblestone streets, horse-drawn carriages and pastel antebellum houses, particularly in the elegant French Quarter and Battery districts.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Asheville, NC',
    description: `Asheville is a city in western North Carolina’s Blue Ridge Mountains. It’s known for a vibrant arts scene and historic architecture, including the dome-topped Basilica of Saint Lawrence. The vast 19th-century Biltmore estate displays artwork by masters like Renoir.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Buffalo, NY',
    description: `Buffalo is a city on the shores of Lake Erie in upstate New York. Its fine neoclassical, beaux arts and art deco architecture speaks to its history as an industrial capital in the early 20th century.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Canton, OH',
    description: `Canton is a city in northeast Ohio. It's known for the Pro Football Hall of Fame, which honors National Football League players through permanent and traveling exhibitions. The circular, domed William McKinley Tomb is the final resting place of the 25th U.S. president, who spent much of his life in Canton.`,
    img: 'city_search_2.png'
  },
  {
    name: 'Fort Worth, TX',
    description: `Fort Worth is a city in North Central Texas. In the late 19th century, it became an important trading post for cowboys at the end of the Chisholm Trail. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Fort Lee, NJ',
    description: `Fort Lee is a borough at the eastern border of Bergen County, New Jersey, United States, situated on the Hudson Waterfront atop the Hudson Palisades. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Santa Fe, NM',
    description: `Santa Fe, New Mexico’s capital, sits in the Sangre de Cristo foothills. It’s renowned for its Pueblo-style architecture and as a creative arts hotbed. Founded as a Spanish colony in 1610, it has at its heart the traditional Plaza. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Detroit, MI',
    description: `Detroit is the largest city in the midwestern state of Michigan. Near Downtown, the neoclassical Detroit Institute of Arts is famed for the Detroit Industry Murals painted by Diego Rivera, and inspired by the city’s ties to the auto industry, giving it the nickname "Motor City."`,
    img: 'city_search_2.png'
  },
  {
    name: 'Ann Arbor, MI',
    description: `Ann Arbor is a city west of Detroit, in the Midwestern state of Michigan. It’s home to the sprawling University of Michigan, known for its research programs. The University of Michigan Museum of Art displays works from around the globe and spanning centuries. `,
    img: 'city_search_2.png'
  },
  {
    name: 'Boston, MA',
    description: `Boston is the capital and most populous city of the Commonwealth of Massachusetts in the United States, as well as the 21st most populous city in the United States. The city proper covers 48 square miles (124 km2) with an estimated population of 694,583 in 2018`,
    img: 'city_search_2.png'
  }
]