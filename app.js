const search__content = document.querySelectorAll('.search__content')
const borderBottomAnimateDiv = document.querySelectorAll('.border-bottom-animate-div')
const header__link = document.querySelectorAll('.header__link--anchor')
const container = document.querySelector('.container')
const searchModalContainer = document.querySelector('.search__modal-container')
const searchModalContentContainer = document.querySelector('.search__modal__content-container')
const search__pickupLocationInput = document.querySelector('.search__pickupLocation-input')
const searchResultsLeftContent = document.querySelector('.search-results-left-content')
const searchResultsRightContent = document.querySelector('.search-results-right-content')
const searchResultsLeft = document.querySelector('.search-results-left')
const searchResultsRightImg = document.querySelector('.search-results-right-img')
const searchResultsRightDescription = document.querySelector('.search-results-right-description')
const dateUI = document.querySelector('.date-ui'); 

const appState = {
  selection: []
}

const animateBorder = (borderTo) => {
  borderBottomAnimateDiv.forEach(element => {
    element.classList.remove('border-bottom-animate-div-show')
  });
  borderTo.classList.add('border-bottom-animate-div-show')
}

const handleClick = (element) => {
  if(element.target.parentElement.classList.contains('search__content')) {
    if(!searchModalContainer.classList.contains('search__modal-container-show')) {
      searchModalContainer.classList.add('search__modal-container-show')
      searchModalContentContainer.classList.add('search__modal__content-container-show')
    }    
    let borderToAnimate = element.target.parentElement.firstElementChild.nextElementSibling.nextElementSibling
    animateBorder(borderToAnimate)
    if(element.target.parentElement.classList.contains('search__pickupDay')) {
      showaddToDateUI('pickup')
      console.log('PICK UP DAY')
      return
    }
    if(element.target.parentElement.classList.contains('search__pickupTime')) {
      console.log('PICK UP TIME')
      return
    }
    if(element.target.parentElement.classList.contains('search__dropoffDay')) {
      console.log('DROP OFF DAY')
      return
    }
    if(element.target.parentElement.classList.contains('search__dropoffTime')) {
      console.log('DROP OFF TIME')
      return
    }
  }
}

const handleMenuClick = (e) => {
  const elem = e.target
  header__link.forEach(link => {
    link.classList.remove('active')
  });
  elem.classList.add('active')
}

const addToInputUI = (e) => {
  search__pickupLocationInput.value = e.target.dataset.name
  appState.selection = ''
  appState.selection = search__pickupLocationInput.value
  searchResultsRightImg.src = 'img/' + e.target.dataset.img
  searchResultsRightDescription.innerHTML = `
  <h2>${e.target.dataset.name}</h2>
  <p>${e.target.dataset.description}</p>
  `
  console.log(appState)
}

const showaddToDateUI = (dateType) => {
  if(dateType === 'pickup') {
    console.log(searchResultsLeftContent)
    searchResultsLeftContent.innerHTML = '';
    const pickUpHTML = `
    <section class="date-ui-show">
      <div class="date-ui-wrap">
          <h2>Pick Up Date</h2>
          <input class="date-ui-input" type="date">
      </div>
    </section>
    `
    searchResultsLeftContent.innerHTML = pickUpHTML;
    // dateUI.classList.add('date-ui-show')
    console.log('IN DATE UI')
  }

}


const handleSearch = (e) => {
  let searchTerm = e.target.value.toLowerCase();

  if(searchTerm.length < 3) {
    searchResultsLeftContent.innerHTML = `Please Enter Search Location <i class="fa fa-search"></i>` 
    return
  }
  let filtered = locations.filter((location) => {
    return location.name.toLowerCase().includes(searchTerm) 
  })
  if(filtered.length > 0) {
    searchResultsLeftContent.innerHTML = ''
    const ul = document.createElement('ul')
    ul.classList.add('search-results-left-ul')
    filtered.forEach(element => {
      let li = document.createElement('li')
      li.classList.add('search-results-left-li')
      li.dataset.name = element.name
      li.dataset.description = element.description
      li.dataset.img = element.img
      li.addEventListener('click', addToInputUI)
      li.textContent = element.name
      ul.appendChild(li)
    });
    searchResultsLeftContent.appendChild(ul)
  } else {
    console.log('Sorry No Match here')
    searchResultsLeftContent.innerHTML = ''
    searchResultsLeftContent.innerHTML = 'Sorry No Match Here <i class="fa fa-search"></i>' 
  }
}


search__pickupLocationInput.addEventListener('keyup', handleSearch)

searchModalContainer.addEventListener('click', () => {
  searchModalContainer.classList.remove('search__modal-container-show')
  searchModalContentContainer.classList.remove('search__modal__content-container-show')
})


// Top NAV MENU LINKS
header__link.forEach(element => {
  element.addEventListener('click', handleMenuClick)
});


search__content.forEach(element => {
  element.addEventListener('click', handleClick)
});

searchResultsLeftContent.addEventListener('change', (e)=> {
  if(e.target.classList.contains('date-ui-input')) {
    let p = document.createElement('p');
    p.dataset.pickDate = e.target.value;
    p.textContent = 'Pick-Up Date: ' + e.target.value;
    searchResultsRightContent.appendChild(p)
  }
})